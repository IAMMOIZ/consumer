import React , { useState } from 'react';
import { View , TextInput , Button ,  Image , Text , TouchableOpacity  , StyleSheet , ImageBackground, Alert } from 'react-native';

const OtpVerificationScreen = ({ navigation }) => {
    //states
    let [ OTP , setOTP ] = useState(0)
    
    const resendOtpHandler = ()=>{
        //api to resend otp
        setOTP(0)
        console.log("otp resend");
    }


    const onPressHandler = () => {
        // Alert.alert('Button Pressed!');
        // console.log("navigate to the otp verification screen");
        // navigation.navigate("otpScreen");
        // console.log(number);
        console.log( OTP );
        if( OTP == 1233 )
        {
            console.log("mobile number is right navigate to the otp verification screen" , OTP);
            setOTP(0)
            navigation.navigate("mainScreen");
        }
        else
        {
            Alert.alert(
                "Warning" ,
                 "OTP verification failed. Please Enter Valid Code !" , 
                 [
                {
                  text: "OK",
                  // onPress: () => Alert.alert("OK Pressed"),
                  // style: "",
                },
                {
                    text: "Resend OTP",
                    onPress:  resendOtpHandler ,
                    // style: "",
                }
              ],)
        }
        
    }


        return (
            <View style={styles.container}> 
                <ImageBackground source={require('../assets/otp-bg.png')} style={styles.bakcgroundImage}>
                    <View style={styles.subContainer}>
                    <Image source={require('../assets/slider.png')} style={styles.image} />
                    <Text style={{ fontSize : 35 , marginBottom : 30 }}>Enter OTP</Text>
                        <TextInput 
                            style={styles.input}
                            value={ OTP }
                            placeholder="Enter Your OTP"
                            keyboardType="numeric"
                            maxLength={4}
                            onChangeText = { setOTP } />
                            <TouchableOpacity onPress={ resendOtpHandler }>
                                <Text style={{ color : "blue" }}>Resend OTP ?</Text>
                            </TouchableOpacity>
                        <TouchableOpacity style={ styles.sendButton} onPress={ onPressHandler } >
                            <Text>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );
    }


export default OtpVerificationScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subContainer : {
        flex : 1,
        alignItems : "center",
        justifyContent : "center"
    },
    sendButton : {
        width : "40%",
        height : 50 ,
        backgroundColor : '#7ed6df',
        alignItems : "center",
        justifyContent : "center",
        borderRadius : 40,
        marginTop : 30
    },
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    input : {
        marginVertical : 10,
        borderColor : "#48dbfb",
        borderWidth : 5,
        height: 50,
        width : "80%",
        padding: 10,
        backgroundColor: '#7ed6df',
        borderRadius: 50,
        marginHorizontal : 2,
    },
    image:{
        marginHorizontal: 100,
        width: "80%",
        height: "40%",
        alignItems: 'center',
        justifyContent:'center'
    },
});
