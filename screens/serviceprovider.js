import * as React from 'react';
import { ScrollView , View , Image , Text , TouchableOpacity  , StyleSheet, TextInput} from 'react-native';
    
const serviceProviderDetailScreen = ({ navigation }) => {        
    // const ResendOTP = () => {
    //     // Alert.alert('Button Pressed!');
    //     console.log(" otp resend function");
    //     // navigation.navigate("verify");
    // }

    const toServiceDetailScreen = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to service provider detail screen");
        navigation.navigate("serviceProviderDetail");
    }

    const services = [ { id: 1 , name : "moiz" , rating : 5 , image : require("../assets/broken-cable.png")} 
    , { id: 2 , name : "sameer" , rating : 5 , image : require("../assets/broken-cable.png")} 
    , { id: 3 , name : "afridi" , rating : 5 , image : require("../assets/broken-cable.png")}
    , { id: 4 , name : "shahid" , rating : 5 , image : require("../assets/broken-cable.png")}
    , { id: 5 , name : "ajmal" , rating : 5 , image : require("../assets/broken-cable.png")}
    , { id: 6 , name : "asif" , rating : 5 , image : require("../assets/broken-cable.png")}
    , { id: 7 , name : "asif" , rating : 5 , image : require("../assets/broken-cable.png")}
    , { id: 8 , name : "asif" , rating : 5 , image : require("../assets/broken-cable.png")}]


return (
    <View style={{flex: 1}}>
    <View style={{flex: 0.9}}>
    <ScrollView>
                        <View>
                            <TextInput type="text" style={styles.inputField} placeholder="search...."/>
                            <View style={styles.serviceContainer}>
                            {
                                services.map((service)=>(
                                    <View key={service.id} style={styles.serviceEachContainer}>
                                    <TouchableOpacity onPress={ toServiceDetailScreen }>
                                        <Image source={  service.image  }  style={{ width : 50 , height : 50 }}/>
                                        <Text  >{service.name}</Text>
                                        <Text  >{service.rating}</Text>
                                        </TouchableOpacity>
                                    </View>                        
                                ))
                            }
                            </View>
                             </View>     
                </ScrollView>
       
    </View>
    <View style={{flex: 0.1}}>
    <TouchableOpacity style={styles.nowButton} onPress={ toServiceDetailScreen } >
                        <Text>Book Now</Text>
                </TouchableOpacity>
                           
       
    </View>
</View>
)
    }


export default serviceProviderDetailScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    creausalImage: {
        flex: 1, 
        width: 10, 
        height: 40
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    },
    nowButton : 
    {
        borderWidth : 2,
        backgroundColor : "pink"
    }
});
