import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const confirmyes=()=>{
    console.log("confirm yes")
    navigation.navigate("confirmok")
}

const confirmno=()=>{
    console.log("confirm no")
    navigation.navigate("confirmnot")
}

export default function App10({ navigation }) {
        return(
            <View>
                <Text>Are you sure confirm payment</Text>
                <Button title="Yes" onPress={confirmyes}/>
                <Button title="No" onPress={confirmno}/>
            </View>
        )    
}
const styles = StyleSheet.create({
    page10 :{
        fontSize : 30
    }
})