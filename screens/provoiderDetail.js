import * as React from 'react';
import { ScrollView , View , Image , Text , TouchableOpacity  , StyleSheet, TextInput} from 'react-native';
    
const serviceProviderDetailScreen = ({ navigation }) => {        
    // const ResendOTP = () => {
    //     // Alert.alert('Button Pressed!');
    //     console.log(" otp resend function");
    //     // navigation.navigate("verify");
    // }

    const toServiceDetailScreen = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to service provider detail screen");
        navigation.navigate("serviceProviderDetail");
    }

    const services = [ 
    { id: 1 , name : "electrician" , image : require("../assets/broken-cable.png")} 
    , { id: 2 , name : "plumber" , image : require("../assets/broken-cable.png")} 
    , { id: 3 , name : "gardner" , image : require("../assets/broken-cable.png")}
    , { id: 4 , name : "carpenter" , image : require("../assets/broken-cable.png")}
    , { id: 5 , name : "pest controll" , image : require("../assets/broken-cable.png")}
    , { id: 6 , name : "painter" , image : require("../assets/broken-cable.png")}]

    const PersonDetail = { id : 1 ,
         name : "jack" ,
          info : "jjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjh jjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjhjjh hhkjhjh jjhjkhjh hjhjkhkjhjhj jhjhjkhjhj jhjhllhjhjkhj ijhjhjljhjj hjh h hjhhjhjh" ,
           image : require("../assets/broken-cable.png") ,
            doneProject : 120 ,
             ratting : 5,
              review  : 5,
               reViewList : 
        [ { content : "ghhh ghjjj gh gjhghghh ghghjghg ghgjjghjg hhgghghghjghjghghghhgh hghghghghghj hgjhghghghjg hggjhjghgghghgg hghjgjhghghghjhghjg hghghghj",
         id : 1,
          customerName : "ghghghghhhghghghghg"} ]
    }

return (
    <View>
        <ScrollView>
            <Text>Tilak nager near temple indore</Text>
            <View>
                <Image source={ PersonDetail.image  }  style={{ width : 50 , height : 50 }}/>
                <Text>{ PersonDetail.name  } </Text>
                <Text>{ PersonDetail.info  } </Text>
                <Text>Done Project</Text>
                <Text>{ PersonDetail.doneProject  } </Text>
                <Text>Rating</Text>
                <Text>{ PersonDetail.ratting  } </Text>
                <Text>Total Review</Text>
                <Text>{ PersonDetail.review  } </Text>
                <Text>Review</Text>
                <Text>{ PersonDetail.reViewList[0].content  } </Text>
                <Text>{ PersonDetail.reViewList[0].customerName  } </Text>
                <TouchableOpacity style={styles.nowButton} >
                        <Text>Book Now</Text>
                </TouchableOpacity>
            </View>
            <View>
                     <Text>MORE</Text>
                     <View style={styles.serviceContainer}>
                    {
                        services.map((service)=>(
                            <View key={service.id} style={styles.serviceEachContainer}>
                            <TouchableOpacity>
                                <Image source={  service.image  }  style={{ width : 50 , height : 50 }}/>
                                <Text  >{service.name}</Text>
                                </TouchableOpacity>
                            </View>                        
                        ))
                    }
    
</View>

                 </View>
    </ScrollView>

</View>  

)
    }


export default serviceProviderDetailScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    creausalImage: {
        flex: 1, 
        width: 10, 
        height: 40
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    },
    nowButton : 
    {
        borderWidth : 2,
        backgroundColor : "pink"
    }
});
