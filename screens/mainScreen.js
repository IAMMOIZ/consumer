import * as React from 'react';
import { View, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import service from "./service"
import serviceProviderList from "./serviceprovider"
import serviceProviderDetail from './provoiderDetail';
// function HomeScreen({ navigation }) {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Button
//         title="Go to Settings"
//         onPress={() => navigation.navigate('ServiceProviderList')}
//       />
//     </View>
//   );
// }

function ValletScreen() {
  return <View />;
}

function ProfileScreen() {
  return <View />;
}

function ServiceProviderListScreen({ navigation }) {


    const handler = () =>{
        console.log("yes clicked");
        navigation.navigate('Home')
    }

  return (<View>
      <Button title="click me"   onPress ={ handler }
/>
  </View>);
}

const Tab = createBottomTabNavigator();

function HomeTabs() {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen name="Home" component={service} />
      <Tab.Screen name="Vallet" component={ValletScreen} />
      {/* <Tab.Screen name="Profile" component={ProfileScreen} /> */}
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeTabs} />
      <Stack.Screen name="Service" component={service} />
      <Stack.Screen name="serviceProviderList" component={serviceProviderList} />
      <Stack.Screen name="serviceProviderDetail" component={serviceProviderDetail} />
      {/* <Stack.Screen name="prodetail" component={serviceProviderDetail} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
