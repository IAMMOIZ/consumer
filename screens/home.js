import * as React from 'react';
import { View , ImageBackground , Text , TouchableOpacity  , StyleSheet, Button, SafeAreaView} from 'react-native';
    
const HomeScreen = ({ navigation }) => {        
    const clickHandler = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the otp verification screen");
        navigation.navigate("EnterNumberForFirstLogin");
    } 
        return (
            <View style={ styles.container }>
                <TouchableOpacity style={styles.bakcgroundImage} onPress={clickHandler}>
                    <ImageBackground source={require('../assets/init_background.png')} style={styles.bakcgroundImage}>
                </ImageBackground>
                </TouchableOpacity>
            </View>
);
    }


export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton: {
        marginBottom: '10',
        justifyContent: 'center',
        alignContent:'center'
    }
});
