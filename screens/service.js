import * as React from 'react';
import { ImageBackground , SafeAreaView , ScrollView , View , Image , Text , TouchableOpacity  , StyleSheet, TextInput} from 'react-native';
    
const serviceScreen = ({ navigation }) => {        
    const NavigateToServiceProvider = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to service provider list screen");
        navigation.navigate("serviceProviderList");
    }
    const services = [ { id: 1 , name : "electrician" , image : require("../assets/broken-cable.png")} 
    , { id: 2 , name : "plumber" , image : require("../assets/broken-cable.png")} 
    , { id: 3 , name : "gardner" , image : require("../assets/broken-cable.png")}
    , { id: 4 , name : "carpenter" , image : require("../assets/broken-cable.png")}
    , { id: 5 , name : "pest controll" , image : require("../assets/broken-cable.png")}
    , { id: 6 , name : "painter" , image : require("../assets/broken-cable.png")}]
        return (
            <ScrollView>
            <ImageBackground source={require('../assets/otp-bg.png')} style={styles.bakcgroundImage}>
                {/* crausal */}
                <View style={styles.imageContainerMain}>
                    <View style={styles.imageContainer}>

            <Image source={require('../assets/slider.png')} style={styles.image} />
            <Text style={styles.input}>Get Discount Up To 25%</Text>


                    </View>
                </View>
            {/* search area */}
            <View>
                <TextInput style={styles.inputField} type="text" placeholder="Search..."/>
            </View>
            <View style={styles.serviceContainer}>
                {
                    services.map((service)=>(
                        <View key={service.id} style={styles.serviceEachContainer}>
                        <TouchableOpacity onPress={ NavigateToServiceProvider }>
                            <Image source={  service.image  }  style={{ width : 50 , height : 50 }}/>
                            <Text>{service.name}</Text>

                            </TouchableOpacity>

                        </View>                        
                    ))
                }
            </View>
            </ImageBackground>
            </ScrollView>
           );
    }

export default serviceScreen

const styles = StyleSheet.create({
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        marginTop: 0,
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    creausalImage: {
        flex: 1, 
        width: 10, 
        height: 40
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    },
    serviceContainer : {
        flexDirection : "row",
        flexWrap : "wrap",
        margin:10,
        marginBottom:10,
    },
    serviceEachContainer : {
        width : 120 ,
        height : 150,
        alignItems : 'center',
        borderColor:'#48dbfb',
        borderWidth:2,
        justifyContent:'center',
    },
    image:{
        marginHorizontal: 130,
        width: 200,
        height: 200,
        alignItems: 'flex-end',
        justifyContent: 'flex-end'

    },
    imageContainer:{
        backgroundColor:'#7ed6df',
        width:'60%',
    },
    imageContainerMain:{
        backgroundColor:'blue',
        margin:30,
    },
    input : {
        marginTop : 30,
        height: 40,
        padding: 10,
        alignItems:'center'
    },
    inputField:{
            marginTop : 0,
            borderColor : "#48dbfb",
            borderWidth :5,
            height: 40,
            padding: 10,
            backgroundColor: '#7ed6df',
            borderRadius: 30,
            marginHorizontal :8,
    }
});
