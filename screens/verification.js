import React , { useState } from 'react';
import { View , TextInput , Button ,  Image , Text , TouchableOpacity  , StyleSheet , ImageBackground, Alert } from 'react-native';

const OtpVerificationScreen = ({ navigation }) => {
    //states
    let [ number , setNumber ] = useState()
    
    const onPressHandler = () => {
        // Alert.alert('Button Pressed!');
        // console.log("navigate to the otp verification screen");
        // navigation.navigate("otpScreen");
        // console.log(number);
        console.log(number );
        if( number == 9000090000 )
        {
            console.log("mobile number is right navigate to the otp verification screen" , number);
            navigation.navigate("otpVerification");
        }
        else
        {
            Alert.alert(
                "Warning" ,
                 "Please Enter Valid Mobile Number" , 
                 [
                {
                  text: "OK",
                  // onPress: () => Alert.alert("OK Pressed"),
                  // style: "",
                },
              ],)
        }
        
    }


        return (
            <View style={styles.container}> 
                <ImageBackground source={require('../assets/otp-bg.png')} style={styles.bakcgroundImage}>
                    <View style={styles.subContainer}>
                    <Image source={require('../assets/slider.png')} style={styles.image} />
                        <Text style={{ fontSize : 35 , marginBottom : 30 }}>Verification</Text>
                        <Text  style={{ fontStyle : "italic" , }} >Enter Your Mobile Number</Text>
                        <TextInput 
                            style={styles.input}
                            value={ number }
                            placeholder="Enter Your Number"
                            keyboardType="numeric"
                            maxLength={10}
                            onChangeText = { setNumber } />
                        <Text>We Will Send OTP On Your Number</Text>
                        <TouchableOpacity style={ styles.sendButton} onPress={ onPressHandler } >
                            <Text>Send OTP</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );
    }


export default OtpVerificationScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subContainer : {
        flex : 1,
        alignItems : "center",
        justifyContent : "center"
    },
    sendButton : {
        width : "40%",
        height : 50 ,
        backgroundColor : '#7ed6df',
        alignItems : "center",
        justifyContent : "center",
        borderRadius : 40,
        marginTop : 60
    },
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    input : {
        marginVertical : 10,
        borderColor : "#48dbfb",
        borderWidth : 5,
        height: 50,
        width : "80%",
        padding: 10,
        backgroundColor: '#7ed6df',
        borderRadius: 50,
        marginHorizontal : 2,
    },
    image:{
        marginHorizontal: 100,
        width: "80%",
        height: "40%",
        alignItems: 'center',
        justifyContent:'center'
    },

});
