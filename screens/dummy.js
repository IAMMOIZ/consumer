import * as React from 'react';
// asdassasfsdfds
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from "./screens/home"
// import profile from "./screens/profile"
import verifyNumber from "./screens/verification"
import otpVerification from "./screens/otpVerification"
import service from "./screens/service";
import MainScreen from "./screens/mainScreen"
// import serviceProviderList from "./screens/serviceprovider";
// import providerdetails from "./screens/provoiderDetail";
// import Quatation from "./screens/cotation";
// import Addtowallet from "./screens/addToWallet";
// import BookingDone from "./screens/payforbooking";
// import jobdisc from "./screens/jobstatus";
// import booking from "./screens/bookingconfirm";
// import cancelbookingreason from "./screens/cancelreason";
// import visitpendingpage from "./screens/visitpending";
// import AcceptQuatation from "./screens/acceptquatation";
// import WorkingDone from "./screens/workdone";
// import ReviewScreen from "./screens/reviewscreen";
// import BlankScreen from "./screens/extrascreen";
// import RegistrationScreen from "./screens/providerRegistration"
// import LoginScreen  from "./screens/loginScreen"


const Stack = createNativeStackNavigator();
// const Drawer = createDrawerNavigator();
const App = () => {

  // return (
  //   <MainScreen/>
  // )


  return (
    <NavigationContainer   >
      <Stack.Navigator independent={ true }>
        {/* done }
        <Stack.Screen
          name="mainScreen"
          component={ mainScreen }
          options={{ headerShown : false }}
        />*/}
      <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown : false }}

        />
        <Stack.Screen
          name="EnterNumberForFirstLogin"
          component={ verifyNumber }
          options={{ headerShown : false }}
        />
        

        {/* done }
      <Stack.Screen
          name="Registeration"
          component={RegistrationScreen}
          options={{ headerShown : false }}
        />
        {/* process  
        <Stack.Screen
          name="service"
          component={service}
          options={{ headerShown : false }}
        />
        {*
        <Stack.Screen 
          name="verify" 
          component={verify}
          options={{ headerShown : false }}
          />
        */}
        <Stack.Screen 
          name="otpVerification" 
          component={otpVerification}
          options={{ headerShown : false }}
          />
          {/*}
        <Stack.Screen 
          name="serviceProviderList" 
          component={ serviceProviderList }
          options={{ headerShown : false }}
          />
        <Stack.Screen 
          name="prodetail" 
          component={providerdetails}
          options={{ headerShown : false }}
          />
        <Stack.Screen 
          name="cotation" 
          component={Quatation}
          options={{ headerShown : false }}
          />
        <Stack.Screen 
          name="walletpay" 
          component={Addtowallet}
          options={{ headerShown : false }}
          />
        <Stack.Screen 
          name="walletcancel" 
          component={Addtowallet}/>
        <Stack.Screen 
          name="bookok" 
          component={BookingDone}/>
        <Stack.Screen 
          name="bookcancel" 
          component={BookingDone}/>
        <Stack.Screen 
          name="booked" 
          component={jobdisc}/>
        <Stack.Screen 
          name="confirmok" 
          component={booking}/>
        <Stack.Screen 
          name="confirmnot" 
          component={booking}/>
        <Stack.Screen 
          name="cancelreason" 
          component={cancelbookingreason}/>
        <Stack.Screen 
          name="visitpending" 
          component={visitpendingpage}/>
        <Stack.Screen 
          name="accept" 
          component={AcceptQuatation}/>
        <Stack.Screen 
          name="workdone" 
          component={WorkingDone}/>
        <Stack.Screen 
          name="reviewscreen" 
          component={ReviewScreen}/>
        <Stack.Screen 
          name="new" 
          component={BlankScreen}/>
          */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

{/* <Drawer.Navigator initialRouteName="Home">
<Drawer.Screen name="Home" component={Home} />
<Drawer.Screen name="Notifications" component={otpVerification} />
</Drawer.Navigator> */}
