import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const review=()=>{
    console.log("ok");
    navigation.navigate("reviewscreen");
}

export default function App14({ navigation }) {
    return(
        <View>
            <Text style={styles.page14}>this is review screen</Text>
            <Button title="Done" onPress={review}/>
        </View>
    );
}

const styles=StyleSheet.create({
    page14 :{
        color : "red"
    }
})