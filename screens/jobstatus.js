import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const status=()=>{
    console.log("confirm");
    navigation.navigate("booked");
}

export default function App9({ navigation }) {
    return(
        <View>
            <Text style={styles.page9}>click to confirm booking</Text>
            <Button title="Cancel" onPress={status}/>
        </View>
    );
}

const styles=StyleSheet.create({
    page8 :{
        color : "red"
    }
})