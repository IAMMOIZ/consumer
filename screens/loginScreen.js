// import * as React from 'react';
import React, {useState , useEffect} from 'react';
import {  SafeAreaView , TextInput , Button , View , Image , Text , TouchableOpacity  , StyleSheet, Alert , ImageBackground } from 'react-native';
import axios from 'axios';
import { userLogin } from "../logics"


const LoginScreen = ({ navigation }) => {
    
    // all states
    let [ userName , setUserName ]  = useState("");
    let [ password , setPassword ]  = useState("");

    //event handler function
    const onSignUpClick = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the registaration screen");
        // axios.get('https://homefixit.in/getServiceCategories')
        //     .then(function (response) {
        //         console.log("success",response.data , response.status);
        //     })
        //     .catch(function (error) {
        //         console.log("error" ,error);
        //     });
        navigation.navigate("Registeration");
    } 


    const loginHandler = ()=>{
        console.log(  userName , password  );
        userLogin( {
            email : userName ,
            password : password
          } )
          .then(( result ) => {
              console.log("result" , result.result);
              navigation.navigate("service");
          })
          .catch(( error )=>{
              console.log("error",error);
              Alert.alert(
                  "Warning",
                  "something went wrong - api problem",
                  [
                      {
                          text : "Try Again",
                          onPress : ()=>{},
                          style : ""
                      },
                      {
                        text : "SignUp",
                        onPress : ()=>{
                            navigation.navigate("Registeration")
                        },
                        style : ""
                    }
                  ]
              )
          })
    }

    const onForgetPasswordClick = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the registaration screen");
        // axios.get('https://homefixit.in/getServiceCategories')
        //     .then(function (response) {
        //         console.log("success",response.data , response.status);
        //     })
        //     .catch(function (error) {
        //         console.log("error" ,error);
        //     });
        navigation.navigate("Registeration");
    } 

    //effect handling and api calling    
    // useEffect(()=>{
    //     console.log("hiiiii");
    //     getServices()
    //     .then((result)=>{
    //         //services data 
    //         console.log(result.data);
    //         // setServices(result.data.result)
    //     })
    //     .catch((error)=>{
    //         Alert.alert(
    //             "Warning",
    //             "Something went wrong - api failure",
    //             [
    //               {
    //                 text: "OK",
    //                 // onPress: () => Alert.alert("OK Pressed"),
    //                 // style: "",
    //               },
    //             ],
    //             // {
    //             //   cancelable: true,
    //             //   onDismiss: () =>
    //             //     Alert.alert(
    //             //       "This alert was dismissed by tapping outside of the alert dialog."
    //             //     ),
    //             // }
    //           )
    //     })
    //     // setName(response.data.name)
    // } , [ name ])


        return (
            <ImageBackground source={require('../assets/otp-bg.png')} style={styles.bakcgroundImage}>

            <SafeAreaView style={styles.container}>
            <Image source={require('../assets/slider.png')} style={styles.image} />

                 <TextInput
                    style={styles.input}
                    value={ userName }
                    placeholder="Enter Your UserName OR Email"
                    onChangeText = { setUserName }
                />

                <TextInput
                    style={styles.input}
                    value={ password }
                    secureTextEntry={true}
                    placeholder="Enter Your password"
                    onChangeText = { setPassword }
                />
                <TouchableOpacity onPress={ onForgetPasswordClick } >
                <Text style={styles.text}>Forget Password</Text>
                </TouchableOpacity>
                <Button title="Login"  onPress={ loginHandler }/>

                <TouchableOpacity onPress={ onSignUpClick }>
                    <Text style={styles.text}>New User ? SignIn First</Text>
                </TouchableOpacity>

            </SafeAreaView>
            </ImageBackground>
            
            );
    }


export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        marginTop: 20,
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    loginButton: {
        width:'50%',
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal: 100,
        marginTop: 20,
        borderRadius: 30,
    },
    input : {
        marginTop : 10,
        borderColor : "#48dbfb",
        borderWidth :5,
        height: 40,
        padding: 10,
        backgroundColor: '#7ed6df',
        borderRadius: 30,
        marginHorizontal :7,
    },
    image:{
        marginHorizontal: 100,
        width: 200,
        height: 200,
        alignItems: 'center',
        justifyContent:'center'
    },
    text:{
        color:'#48dbfb',
        textAlign:'right',
        margin: 10

    }
});
