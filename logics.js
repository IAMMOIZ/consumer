import httpConstants from "./constants"
import axios from "axios"
const BASE_URL = "https://homefixit.in/";

// logic 1
export const userSignup = ( data ) => {
    return new Promise((resolve, reject) => {
        // console.log( httpConstants.BASE_URL );
        let url = `${BASE_URL}userSignup`
        // console.log("url",url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                resolve(result)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 2

export const providerSignup = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_SIGNUP}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 3

export const userImageUpload = () => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.USER_IMG_UPLOAD}`
        console.log(url);

        axios
            .post(url)
            .then((result) => {
                return result
            })
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

// logic 4

export const confirmUser = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.USER_REGISTRATION}`
        console.log(url);


        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })
    })
}

// logic 5

export const confirmProvider = ( data ) => {
    return new Promise(() => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_REGISTRATION}`;
        console.log(url);


        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 6

export const userLogin = ( data ) => {
    return new Promise((resolve, reject) => {
        // console.log( httpConstants.BASE_URL );
        let url = `${BASE_URL}loginUser`
        // console.log("url",url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                resolve(result)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })

}

// logic 7

export const loginProvider = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.LOGIN_PROVIDER}`;
        console.log(url)

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 8

export const providerDocUpload = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_DOC_UPLOAD}`;
        console.log(url);


        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })
    })
}

// logic 9

export const resendUserOtp = ( data ) => {
        return new Promise((resolve, reject) => {
            // console.log( httpConstants.BASE_URL );
            let url = `${BASE_URL}resendUserOtp`
            // console.log("url",url);
    
            axios
                .post(url , data )
                .then((result) => {
                    console.log("first");
                    return result
                })
                .catch((error) => {
                    console.log("error");
                    reject(error)
                })
    
        })
    }

// logic 10

export const resendProviderOtp = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_OTP}`;
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                resolve(result)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 11

export const getServices = () => {
    return new Promise((resolve, reject) => {
        let url = `${BASE_URL}getServices`;
        console.log(url);

        axios
            .get(url)
            .then((result) => {
                // console.log(result);
                // console.log("======================");
                resolve(result)
            })
            .catch((error) => {
                // console.log("======================");
                reject(error)
            })
    })
}

// logic 12

export const getServiceCategories = () => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.SERVICES_CATEGORIES}`;
        console.log(url);

        axios
            .get(url)
            .then((result) => {
                return result
            })
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

// logic 13

export const sendUserRequest = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.USER_REQUEST}`;
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 14

export const getAcceptingProvider = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.ACCEPT_PROVIDER}`;
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 15

export const getUsersRequest = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.USER_REQUEST_TO_PROVIDER}`;
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 16

export const acceptUsersRequest = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_ACCEPT_REQUEST}`;
        Console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 17

export const confirmVisitCount = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.CONFIRM_PROVIDER}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 18

export const feedbackRating = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.FEEDBACK}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 19

export const createPayment = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.CREATE_PAYMENT}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 20

export const verifyPayment = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.VERIFY_PAYMENT}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 21

export const createWalletPayment = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.CREATE_WALLET_PAYMENT}`
        console.log(url);


        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 22

export const verifyWalletPayment = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.VERIFY_WALLET_PAYMENT}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}


// logic 23

export const createQuotation = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.QUOTATION}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 24

export const changePassword = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.CHANGE_PASSWORD}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}

// logic 25

export const getUserHistory = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.HISTORY}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 26

export const providerBankdetails = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.INSERT_PROVIDER_BANK_DETAILS}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 27

export const getProviderbankdetails = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.GET_PROVIDER_BANK_DETAILS}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 28

export const getChargelist = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.SERVICE_CHARGE_LIST}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 29

export const ProviderChargelist = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.PROVIDER_CHARGE_LIST}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })


    })
}

// logic 30

export const userCancelRequest = ( data ) => {
    return new Promise((resolve, reject) => {
        let url = `${httpConstants.BASE_URL}${httpConstants.CANCEL_REQUEST}`
        console.log(url);

        axios
            .post(url , data )
            .then((result) => {
                console.log("first");
                return result
            })
            .then((data) => {
                console.log("second");
                resolve(data)
            })
            .catch((error) => {
                console.log("error");
                reject(error)
            })

    })
}